This Dockerfile provides a Jekyll that is based on Debian testing with
Debians version of gem.

It installs all the bundles in `/home/jekyll/bundle` and it comes with the
following gems preinstalled:

* webrick
* html-proofer
* sass-embedded
* s3_website
* jekyll-sitemap
* jekyll-mentions
* jekyll-coffeescript
* jekyll-sass-converter
* jekyll-redirect-from
* jekyll-paginate
* jekyll-compose
* jekyll-feed
* jekyll-docs
* RedCloth
* kramdown
* jemoji
* minima

The default entrypoint is `jekyll` and it starts on port 4000.

If you base of this image you can do something like this in your Dockerfile:

```Dockerfile
FROM ...

COPY --chown=jekyll:jekyll src/Gemfile* /srv/jekyll/
RUN bundle install
COPY --chown=jekyll:jekyll src/ /srv/jekyll/
```

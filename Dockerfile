#
# SPDX-FileCopyrightText: 2023 Wesley Schwengle <wesley@opndev.io>
#
# SPDX-License-Identifier: BSD-3-Clause
#
# This Dockerfile provides a jekyll that is based on Debian testing with
# Debians version of gem.
#
FROM debian:testing

LABEL io.opndev.vendor="OPN Development"
LABEL io.opndev.label-with-value="jekyll"
LABEL version="1.0"
LABEL description="Custom jekyll image"
LABEL maintainer="Wesley Schwengle <wesley@opndev.io>"

RUN <<OEF
apt-get update
apt-get upgrade -y
apt-get install -y --no-install-recommends \
  ruby \
  ruby-dev \
  make \
  libffi-dev \
  gcc \
  libc6-dev \
  g++ \
  build-essential

# gem update --system
# We can do this if we base of the ruby image
gem install bundler
gem cleanup

groupadd -g 1000 jekyll
useradd -ms /bin/bash -u 1000 -g 1000 jekyll -d /home/jekyll
mkdir -p /srv/jekyll
chown -R jekyll:jekyll /srv/jekyll
OEF

USER jekyll
ARG BUNDLE_PATH=/home/jekyll/bundle
ENV BUNDLE_PATH=$BUNDLE_PATH
WORKDIR /srv/jekyll

RUN <<OEF
# Install everything and than leave no trace
bundle init
bundle add \
  jekyll \
  webrick \
  html-proofer \
  sass-embedded \
  s3_website \
  jekyll-sitemap \
  jekyll-mentions \
  jekyll-coffeescript \
  jekyll-sass-converter \
  jekyll-redirect-from \
  jekyll-paginate \
  jekyll-compose \
  jekyll-feed \
  jekyll-docs \
  RedCloth \
  kramdown \
  jemoji \
  minima
rm Gemfile*
OEF

ENTRYPOINT [ "bundle", "exec", "jekyll" ]
CMD [ "serve", "-H", "0.0.0.0", "-P", "4000", "--trace" ]

#FROM base AS jekyll
#COPY --chown=jekyll:jekyll src/Gemfile* /srv/jekyll/
#RUN bundle install
#COPY --chown=jekyll:jekyll src/ /srv/jekyll/
